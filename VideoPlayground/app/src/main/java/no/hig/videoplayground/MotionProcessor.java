package no.hig.videoplayground;

import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;
import org.opencv.video.BackgroundSubtractorMOG2;
import org.opencv.video.Video;


/**
 * This class is a controller for puzzle game.
 * It converts the image from Camera into the shuffled image
 */
public class MotionProcessor {

    private static final String TAG = "MotionProcessor";


    private BackgroundSubtractorMOG2 sub = Video.createBackgroundSubtractorMOG2();
    private Mat mGray = new Mat();
    private Mat mRgb = new Mat();
    private Mat mFGMask = new Mat();


    private String filename = "test_video.mov";



    public MotionProcessor() {
    }


    /**
     * Prepares the next frame bazed on the inputPicture
     */
    public synchronized Mat processFrame(Mat m) {
        Imgproc.cvtColor(m, mRgb, Imgproc.COLOR_RGBA2RGB); //the apply function will throw the above error if you don't feed it an RGB image
        sub.apply(m, mFGMask, 0.8); //apply() exports a gray image by definition
        Imgproc.cvtColor(mFGMask, m, Imgproc.COLOR_GRAY2RGBA);

        return m;
    }

}

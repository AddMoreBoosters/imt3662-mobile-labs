package no.hig.videoplayground;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener;
import org.opencv.android.JavaCameraView;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Mat;

/**
 *
 */
public class VideoPlayground extends Activity implements CvCameraViewListener, View.OnTouchListener {

    private static final String  TAG = "VideoPlayground";

    private CameraBridgeViewBase mOpenCvCameraView;
    private MenuItem             mItemSettings;
    private MotionProcessor      mMotionProcessor;


    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {

        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                    mMotionProcessor = new MotionProcessor();

                    Log.i(TAG, "OpenCV loaded successfully");

                    /* Now enable camera view to start receiving frames */
                    mOpenCvCameraView.setOnTouchListener(VideoPlayground.this);
                    mOpenCvCameraView.enableView();
                    break;


                default:
                    super.onManagerConnected(status);
                    break;
            }
        }
    };


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.videoplayground);

        this.mOpenCvCameraView = (CameraBridgeViewBase) new JavaCameraView(this, -1);
        this.mOpenCvCameraView.setCvCameraViewListener(this);

        final LinearLayout l = (LinearLayout) findViewById(R.id.bottomLayout);
        l.addView(mOpenCvCameraView);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }


    @Override
    public void onPause() {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    public void onResume() {
        super.onResume();
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, mLoaderCallback);
    }


    public void onDestroy() {
        super.onDestroy();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.i(TAG, "called onCreateOptionsMenu");
        this.mItemSettings = menu.add("Settings");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.i(TAG, "Menu Item selected " + item);
        if (item == mItemSettings) {
        	final Intent i = new Intent(this, SettingsActivity.class);
        	startActivity(i);
        }
        return true;
    }


    @Override
    public void onCameraViewStarted(int width, int height) {
    }

    @Override
    public void onCameraViewStopped() {
    }


    public boolean onTouch(View view, MotionEvent event) {
        return false;
    }

    @Override
    public Mat onCameraFrame(Mat inputFrame) {
        return this.mMotionProcessor.processFrame(inputFrame);
    }
}

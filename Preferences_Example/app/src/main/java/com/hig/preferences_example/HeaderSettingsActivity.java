package com.hig.preferences_example;

import android.preference.PreferenceActivity;

import java.util.List;

public class HeaderSettingsActivity extends PreferenceActivity {


    @Override
    public void onBuildHeaders(List<Header> target) {
        super.onBuildHeaders(target);
        loadHeadersFromResource(R.xml.preference_headers, target);
    }

    /**
     * Need to override to test against fragment injection.
     * @param fragmentName of fragment
     * @return true if valid, false if not
     */
    @Override
    protected boolean isValidFragment(String fragmentName) {
        if(HeaderSettingsFragment.class.getName().equals(fragmentName)){
            return true;
        }

        if(SettingsFragment.class.getName().equals(fragmentName)){
            return true;
        }

        return false;
    }
}
